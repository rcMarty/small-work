# "Malá" práce

## Authors

- Martin Korotwitschka (KOR0289)
- Barbora Kovalská (KOV0354)

## Installation

```bash
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

and run it with `mkdocs serve`

## TODO

Připravte repozitář na Gitlabu, který bude používat generátor statických stránek ve spojení s kontinuální integrací (CI). CI bude generovat HTML stránky z Markdown podkladu. Součástí CI by měl být také test kvality Markdown souborů. Práce vychází z [cvičení č. 6](06_handson.md), kde jsou uvedeny i případné vhodné kontejnery.

Požadavky na kvalitu práce (hodnotící kritéria):

- [x] GIT repozitář na Gitlabu.
  - [x] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak se instaluje, jaké jsou prerekvizity, apod.)
- [x] Použití libovolného generátoru statických stránek dle vlastního výběru. (MKdocs, Middleman, Jekyll, apod.)
  - použili jsme mkdocs
- [x] Vytvořená CI v repozitáři.
- [x] CI má minimálně dvě úlohy:
  - [x] Test kvality Markdown stránek.
  - [x] Generování HTML stránek z Markdown zdrojů.
- [ ] CI má automatickou úlohou nasazení web stránek (deploy).
- [ ] Gitlab projekt má fukční web stránky s generovaným obsahem na URL:

- [x] Upéct chleba
